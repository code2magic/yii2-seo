Yii2 seo
==========

## Installation

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run
```
php composer.phar require code2magic/yii2-seo --prefer-dist
```
or add
```
"code2magic/yii2-seo": "*"
```

to the `require` section of your `composer.json` file.

## Usage
