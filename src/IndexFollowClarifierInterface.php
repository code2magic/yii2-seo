<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo;

/**
 * Interface IndexFollowClarifierInterface
 * @package code2magic\seo
 */
interface IndexFollowClarifierInterface
{
    /**
     * IndexFollowClarifierInterface constructor.
     * @param string $route
     * @param array $params
     */
    public function __construct(string $route, array $params);

    /**
     * @return bool
     */
    public function isIndex(): bool;

    /**
     * @return bool
     */
    public function isFollow(): bool;
}