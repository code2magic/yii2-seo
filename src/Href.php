<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo;

use yii\helpers\Url;

/**
 * Class Href
 * @package code2magic\seo
 */
final class Href
{
    /**
     * @var
     */
    private static $_current;

    /**
     * @return string
     */
    private static function getCurrent()
    {
        if (self::$_current === null) {
            self::$_current = Url::current();
        }
        return self::$_current;
    }

    /**
     * @param $url
     * @return string
     */
    private static function getRelNofollow($url)
    {
        if ($url === self::getCurrent()) {
            return 'rel="nofollow"';
        }
        return '';
    }

    /**
     * @param $data
     * @return string
     */
    public static function relNofollowByData($data)
    {
        return self::getRelNofollow(Url::toRoute($data));
    }

    /**
     * @param $url
     * @return string
     */
    public static function relNofollowByUrl($url)
    {
        return self::getRelNofollow($url);
    }
}