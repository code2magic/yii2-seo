<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo\ldjson;

use code2magic\seo\ldjson\interfaces\IPostalAddress;

/**
 * Class PostalAddress
 * @package code2magic\seo\ldjson
 */
class PostalAddress extends BaseLdJson implements IPostalAddress
{
    /**
     * @return string
     */
    public function getType(): string
    {
        return 'PostalAddress';
    }

    /**
     * @param $street
     * @return mixed|void
     */
    public function setStreetAddress($street)
    {
        $this->setDataInternal('streetAddress', $street);
    }

    /**
     * @param $address
     * @return mixed|void
     */
    public function setAddressLocality($address)
    {
        $this->setDataInternal('addressLocality', $address);
    }

    /**
     * @param $address
     * @return mixed|void
     */
    public function setAddressRegion($address)
    {
        $this->setDataInternal('addressRegion', $address);
    }

    /**
     * @param $country
     * @return mixed|void
     */
    public function setAddressCountry($country)
    {
        $this->setDataInternal('addressCountry', $country);
    }

    /**
     * @param $code
     * @return mixed|void
     */
    public function setPostalCode($code)
    {
        $this->setDataInternal('postalCode', $code);
    }
}
