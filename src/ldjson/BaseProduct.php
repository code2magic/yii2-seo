<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo\ldjson;

use code2magic\seo\ldjson\interfaces\IAggregateOffer;
use code2magic\seo\ldjson\interfaces\IAggregateRating;
use code2magic\seo\ldjson\interfaces\IOffer;
use code2magic\seo\ldjson\interfaces\IProduct;
use code2magic\seo\ldjson\interfaces\IThing;

/**
 * Class BaseProduct
 * @package code2magic\seo\ldjson
 */
abstract class BaseProduct extends BaseLdJson implements IProduct
{
    /**
     * @return string
     */
    public function getType(): string
    {
        return 'Product';
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->getDataInternal('name');
    }

    /**
     * @param string $name
     * @return mixed|void
     */
    public function setName(string $name)
    {
        $this->setDataInternal('name', $name);
    }

    /**
     * @param IAggregateOffer $aggregateOffer
     * @return mixed|void
     */
    public function setAggregateOffer(IAggregateOffer $aggregateOffer)
    {
        $this->setDataInternal('offers', $aggregateOffer);
    }

    /**
     * @param IAggregateRating $aggregateRating
     * @return mixed|void
     */
    public function setAggregateRating(IAggregateRating $aggregateRating)
    {
          $this->setDataInternal('aggregateRating', $aggregateRating);
    }

    /**
     * @param array $images
     * @return mixed|void
     */
    public function setImages(array $images)
    {
        $this->setDataInternal('image', $images);
    }

    /**
     * @param string $description
     * @return mixed|void
     */
    public function setDescription(string $description)
    {
        $this->setDataInternal('description', $description);
    }

    /**
     * @param string $mpn
     * @return mixed|void
     */
    public function setMpn(string $mpn)
    {
        $this->setDataInternal('mpn', $mpn);
    }

    /**
     * @param string $sku
     * @return mixed|void
     */
    public function setSku(string $sku)
    {
        $this->setDataInternal('sku', $sku);
    }

    /**
     * @param IThing $thing
     * @return mixed|void
     */
    public function setBrand(IThing $thing)
    {
        $this->setDataInternal('brand', $thing);
    }

    /**
     * @param IOffer $offer
     * @return mixed|void
     */
    public function setOffer(IOffer $offer)
    {
        $this->setDataInternal('offers', $offer);
    }

    /**
     * @param \code2magic\seo\ldjson\interfaces\IReview[] $reviews
     *
     */
    public function setReviews(array $reviews): void
    {
        $this->setDataInternal('review', $reviews);
    }
}
