<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo\ldjson;

use code2magic\seo\ldjson\interfaces\IListItem;

/**
 * Class ListItem
 * @package code2magic\seo\ldjson
 */
class ListItem extends BaseLdJson implements IListItem
{
    /**
     * @return string
     */
    public function getType(): string
    {
        return 'ListItem';
    }

    /**
     * @param int $number
     * @return mixed|void
     */
    public function setPosition(int $number)
    {
        $this->setDataInternal('position', $number);
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return $this->getDataInternal('position');
    }

    /**
     * @param string $name
     * @return mixed|void
     */
    public function setName(string $name)
    {
        $this->setDataInternal('name', $name);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->getDataInternal('name');
    }

    /**
     * @return string
     */
    public function getItem(): string
    {
        return $this->getDataInternal('item');
    }

    /**
     * @param string $item
     * @return mixed|void
     */
    public function setItem(string $item)
    {
        $this->setDataInternal('item', $item);
    }
}
