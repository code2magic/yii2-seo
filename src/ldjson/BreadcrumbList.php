<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo\ldjson;

use code2magic\seo\ldjson\interfaces\IBreadcrumbList;

/**
 * Class BreadcrumbList
 * @package code2magic\seo\ldjson
 */
class BreadcrumbList extends BaseLdJson implements IBreadcrumbList
{
    /**
     * @return string
     */
    public function getType(): string
    {
        return 'BreadcrumbList';
    }

    /**
     * @param \code2magic\seo\ldjson\interfaces\IListItem[] $listItem
     *
     */
    public function setItemListElement(array $listItem)
    {
        $this->setDataInternal('itemListElement', $listItem);
    }

    /**
     * @return \code2magic\seo\ldjson\interfaces\IListItem[]
     */
    public function getItemListElement(): array
    {
        return $this->getDataInternal('itemListElement');
    }
}
