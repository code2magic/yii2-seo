<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo\ldjson;

use code2magic\seo\ldjson\interfaces\IPerson;

/**
 * Class Person
 * @package code2magic\seo\ldjson
 */
class Person extends BaseLdJson implements IPerson
{
    /**
     * @return string
     */
    public function getType(): string
    {
        return 'Person';
    }

    /**
     * @param $name
     * @return mixed|void
     */
    public function setName($name)
    {
        $this->setDataInternal('name', $name);
    }

    /**
     * @return mixed|void
     */
    public function getName()
    {
        $this->getDataInternal('name');
    }
}
