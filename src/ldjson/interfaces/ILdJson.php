<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo\ldjson\interfaces;

/**
 * Interface ILdJson
 * @package code2magic\seo\ldjson\interfaces
 */
interface ILdJson extends \JsonSerializable
{
    /**
     * @return array
     */
    public function getLdJsonArray(): array;

    /**
     * @return string
     */
    public function getType(): string;

    /**
     * @param bool $isContext
     * @return mixed
     */
    public function setIsContext(bool $isContext);

    /**
     * @return bool
     */
    public function getIsContext(): bool;
}
