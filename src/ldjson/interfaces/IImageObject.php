<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo\ldjson\interfaces;

/**
 * Interface IImageObject
 * @package code2magic\seo\ldjson\interfaces
 */
interface IImageObject extends ILdJson
{
    /**
     * @param $url
     * @return mixed
     */
    public function setUrl($url);
}
