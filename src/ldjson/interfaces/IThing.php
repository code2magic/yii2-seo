<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo\ldjson\interfaces;

/**
 * Interface IThing
 * @package code2magic\seo\ldjson\interfaces
 */
interface IThing extends ILdJson
{
    /**
     * @param $name
     * @return mixed
     */
    public function setName($name);

    /**
     * @return mixed
     */
    public function getName();
}
