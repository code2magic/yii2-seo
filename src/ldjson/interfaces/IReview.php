<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo\ldjson\interfaces;

/**
 * Interface IReview
 * @package code2magic\seo\ldjson\interfaces
 */
interface IReview extends ILdJson
{
    /**
     * @return IPerson
     */
    public function getAuthor(): IPerson;

    /**
     * @param IPerson $author
     * @return mixed
     */
    public function setAuthor(IPerson $author);

    /**
     * @return mixed
     */
    public function getDatePublished();

    /**
     * @param $date
     * @return mixed
     */
    public function setDatePublished($date);

    /**
     * @param $description
     * @return mixed
     */
    public function setDescription($description);

    /**
     * @return mixed
     */
    public function getDescription();

    /**
     * @param IRating $rating
     * @return mixed
     */
    public function setReviewRating(IRating $rating);

    /**
     * @return IRating
     */
    public function getReviewRating(): IRating;
}
