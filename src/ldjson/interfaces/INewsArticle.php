<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo\ldjson\interfaces;

/**
 * Interface INewsArticle
 * @package code2magic\seo\ldjson\interfaces
 */
interface INewsArticle extends ILdJson
{
    /**
     * @param IWebPage $webPage
     * @return mixed
     */
    public function setMainEntityOfPage(IWebPage $webPage);

    /**
     * @param $headline
     * @return mixed
     */
    public function setHeadline($headline);

    /**
     * @param array $image
     * @return mixed
     */
    public function setImage(array $image);

    /**
     * @param $date
     * @return mixed
     */
    public function setDatePublished($date);

    /**
     * @param $date
     * @return mixed
     */
    public function setDateModified($date);

    /**
     * @param IPerson $person
     * @return mixed
     */
    public function setAuthor(IPerson $person);

    /**
     * @param IOrganization $organization
     * @return mixed
     */
    public function setPublisher(IOrganization $organization);

    /**
     * @param $description
     * @return mixed
     */
    public function setDescription($description);
}
