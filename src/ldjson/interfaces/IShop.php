<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo\ldjson\interfaces;

use werewolf8904\cmscontent\frontend\models\Article;

/**
 * Interface IShop
 * @package code2magic\seo\ldjson\interfaces
 */
interface IShop
{
    /**
     * @param $data
     * @return mixed
     */
    public function registerBreadcrumbs($data);

    /**
     * @param array $data
     * @return mixed
     */
    public function registerContactPage($data = []);

    /**
     * @param Article $article
     * @return mixed
     */
    public function registerArticle(Article $article);
}
