<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo\ldjson\interfaces;

/**
 * Interface IOffer
 * @package code2magic\seo\ldjson\interfaces
 */
interface IOffer extends ILdJson
{
    /**
     *
     */
    public CONST IN_STOCK = 'http://schema.org/InStock';

    /**
     *
     */
    public CONST OUT_OF_STOCK = 'http://schema.org/OutOfStock';

    /**
     * @return mixed
     */
    public function getPriceCurrency();

    /**
     * @param $currency
     * @return mixed
     */
    public function setPriceCurrency($currency);

    /**
     * @return mixed
     */
    public function getPrice();

    /**
     * @param $price
     * @return mixed
     */
    public function setPrice($price);

    /**
     * @return mixed
     */
    public function getAvailability();

    /**
     * @param $availability
     */
    public function setAvailability($availability);

    /**
     * @return IOrganization
     */
    public function getSeller(): IOrganization;

    /**
     * @param IOrganization $seller
     */
    public function setSeller(IOrganization $seller);

    /**
     * @return string
     */
    public function getUrl(): string;

    /**
     * @param string $value
     */
    public function setUrl(string $value);

    /**
     * @return string
     */
    public function getPriceValidUntil(): string;

    /**
     * @param string $value
     */
    public function setPriceValidUntil($value): string;

    /**
     * @return string
     */
    public function getItemCondition(): string;

    /**
     * @param string $value
     */
    public function setItemCondition(string $value);
}
