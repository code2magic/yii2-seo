<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo\ldjson\interfaces;

/**
 * Interface IContactPoint
 * @package code2magic\seo\ldjson\interfaces
 */
interface IContactPoint extends ILdJson
{
    /**
     * @param $phone
     * @return mixed
     */
    public function setTelephone($phone);

    /**
     * @return mixed
     */
    public function getTelephone();

    /**
     * @param $phone
     * @return mixed
     */
    public function setContactType($phone);

    /**
     * @return mixed
     */
    public function getContactType();
}
