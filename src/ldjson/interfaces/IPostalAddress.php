<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo\ldjson\interfaces;

/**
 * Interface IPostalAddress
 * @package code2magic\seo\ldjson\interfaces
 */
interface IPostalAddress extends ILdJson
{
    /**
     * @param $street
     * @return mixed
     */
    public function setStreetAddress($street);

    /**
     * @param $address
     * @return mixed
     */
    public function setAddressLocality($address);

    /**
     * @param $address
     * @return mixed
     */
    public function setAddressRegion($address);

    /**
     * @param $country
     * @return mixed
     */
    public function setAddressCountry($country);

    /**
     * @param $code
     * @return mixed
     */
    public function setPostalCode($code);
}
