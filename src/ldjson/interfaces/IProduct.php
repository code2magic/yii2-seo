<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo\ldjson\interfaces;

/**
 * Interface IProduct
 * @package code2magic\seo\ldjson\interfaces
 */
interface IProduct extends ILdJson
{
    /**
     * @param string $name
     * @return mixed
     */
    public function setName(string $name);

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @param IAggregateOffer $aggregateOffer
     * @return mixed
     */
    public function setAggregateOffer(IAggregateOffer $aggregateOffer);

    /**
     * @param IAggregateRating $aggregateRating
     * @return mixed
     */
    public function setAggregateRating(IAggregateRating $aggregateRating);

    /**
     * @param array $images
     * @return mixed
     */
    public function setImages(array $images);

    /**
     * @param string $description
     * @return mixed
     */
    public function setDescription(string $description);

    /**
     * @param string $mpn
     * @return mixed
     */
    public function setMpn(string $mpn);

    /**
     * @param IThing $thing
     * @return mixed
     */
    public function setBrand(IThing $thing);

    /**
     * @param IOffer $offer
     * @return mixed
     */
    public function setOffer(IOffer $offer);

    /**
     * @param \code2magic\seo\ldjson\interfaces\IReview[] $reviews
     *
     */
    public function setReviews(array $reviews): void;
}
