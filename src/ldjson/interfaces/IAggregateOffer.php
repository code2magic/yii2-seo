<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo\ldjson\interfaces;

/**
 * Interface IAggregateOffer
 * @package code2magic\seo\ldjson\interfaces
 */
interface IAggregateOffer extends ILdJson
{
    /**
     * @param string $currency
     * @return mixed
     */
    public function setCurrency(string $currency);

    /**
     * @return string
     */
    public function getCurrency(): string;

    /**
     * @param string $price
     * @return mixed
     */
    public function setLowPrice(string $price);

    /**
     * @return string
     */
    public function getLowPrice(): string;

    /**
     * @param string $price
     * @return mixed
     */
    public function setHighPrice(string $price);

    /**
     * @return string
     */
    public function getHighPrice(): string;

    /**
     * @param string $count
     * @return mixed
     */
    public function setOfferCount(string $count);

    /**
     * @return string
     */
    public function getOfferCount(): string;
}
