<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo\ldjson\interfaces;

/**
 * Interface IListItem
 * @package code2magic\seo\ldjson\interfaces
 */
interface IListItem extends ILdJson
{
    /**
     * @param int $number
     * @return mixed
     */
    public function setPosition(int $number);

    /**
     * @return int
     */
    public function getPosition(): int;

    /**
     * @param string $name
     * @return mixed
     */
    public function setName(string $name);

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return string
     */
    public function getItem(): string;

    /**
     * @param string $item
     * @return mixed
     */
    public function setItem(string $item);
}
