<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo\ldjson\interfaces;

/**
 * Interface IBreadcrumbList
 * @package code2magic\seo\ldjson\interfaces
 */
interface IBreadcrumbList extends ILdJson
{
    /**
     * @param \code2magic\seo\ldjson\interfaces\IListItem[] $listItem
     *
     */
    public function setItemListElement(array $listItem);

    /**
     * @return \code2magic\seo\ldjson\interfaces\IListItem[]
     */
    public function getItemListElement():array;
}
