<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo\ldjson\interfaces;

/**
 * Interface IWebPage
 * @package code2magic\seo\ldjson\interfaces
 */
interface IWebPage extends ILdJson
{
    /**
     * @param $id
     * @return mixed
     */
    public function setId($id);

    /**
     * @return mixed
     */
    public function getId();
}
