<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo\ldjson\interfaces;

/**
 * Interface IOrganization
 *
 * @package code2magic\seo\ldjson\interfaces
 */
interface IOrganization extends ILdJson
{
    /**
     * @param string $name
     *
     * @return mixed
     */
    public function setName(string $name);

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @param string $name
     *
     * @return mixed
     */
    public function setUrl(string $name);

    /**
     * @return string
     */
    public function getUrl(): string;

    /**
     * @param array $same
     *
     * @return mixed
     */
    public function setSameAs(array $same);

    /**
     * @return array
     */
    public function getSameAs(): array;

    /**
     * @param string|\code2magic\seo\ldjson\interfaces\IImageObject $logo
     *
     */
    public function setLogo($logo);

    /**
     * @return string|\code2magic\seo\ldjson\interfaces\IImageObject
     */
    public function getLogo();

    /**
     * @param IPostalAddress $address
     *
     * @return mixed
     */
    public function setPostalAddress(IPostalAddress $address);

    /**
     * @return IPostalAddress
     */
    public function getPostalAddress(): IPostalAddress;

    /**
     * @param \code2magic\seo\ldjson\interfaces\IContactPoint[] $contactPoint
     *
     * @return mixed
     */
    public function setContactPoint(array $contactPoint);

    /**
     * @return \code2magic\seo\ldjson\interfaces\IContactPoint[]
     */
    public function getContactPoint(): array;

    /**
     * @param IAggregateRating $aggregateRating
     */
    public function setAggregateRating(IAggregateRating $aggregateRating);

    /**
     * @return IAggregateRating
     */
    public function getAggregateRating(): IAggregateRating;

    /**
     * @param IReview[] $reviews
     */
    public function setReviews(array $reviews);

    /**
     * @return IReview[]
     */
    public function getReviews();
}
