<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo\ldjson\interfaces;

/**
 * Interface IPerson
 * @package code2magic\seo\ldjson\interfaces
 */
interface IPerson extends ILdJson
{
    /**
     * @param $name
     * @return mixed
     */
    public function setName($name);

    /**
     * @return mixed
     */
    public function getName();
}
