<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo\ldjson\interfaces;

/**
 * Interface IRating
 * @package code2magic\seo\ldjson\interfaces
 */
interface IRating extends ILdJson
{
    /**
     * @return mixed
     */
    public function getBestRating();

    /**
     * @param $rating
     * @return mixed
     */
    public function setBestRating($rating);

    /**
     * @param $rating
     * @return mixed
     */
    public function setRatingValue($rating);

    /**
     * @return mixed
     */
    public function getRatingValue();

    /**
     * @return mixed
     */
    public function getWorstRating();

    /**
     * @param $rating
     * @return mixed
     */
    public function setWorstRating($rating);
}