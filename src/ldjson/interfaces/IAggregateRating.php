<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo\ldjson\interfaces;

/**
 * Interface IAggregateRating
 *
 * @package code2magic\seo\ldjson\interfaces
 */
interface IAggregateRating extends ILdJson
{
    /**
     * @param $value
     */
    public function setRatingValue($value);

    /**
     * @return mixed
     */
    public function getRatingValue();

    /**
     * @param $value
     *
     * @return mixed
     */
    public function setRatingCount($value);

    /**
     * @return mixed
     */
    public function getRatingCount();

    /**
     * @return mixed
     */
    public function getWorstRating();

    /**
     * @param $value
     */
    public function setWorstRating($value);

    /**
     * @return mixed
     */
    public function getBestRating();

    /**
     * @param $value
     */
    public function setBestRating($value);
}
