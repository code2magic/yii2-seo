<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo\ldjson;

use code2magic\seo\ldjson\interfaces\INewsArticle;
use code2magic\seo\ldjson\interfaces\IOrganization;
use code2magic\seo\ldjson\interfaces\IPerson;
use code2magic\seo\ldjson\interfaces\IWebPage;
use Yii;

/**
 * Class NewsArticle
 * @package code2magic\seo\ldjson
 */
class NewsArticle extends BaseLdJson implements INewsArticle
{
    /**
     * @return string
     */
    public function getType(): string
    {
        return 'NewsArticle';
    }

    /**
     * @param IWebPage $webPage
     * @return mixed|void
     */
    public function setMainEntityOfPage(IWebPage $webPage)
    {
        $this->setDataInternal('mainEntityOfPage', $webPage);
    }

    /**
     * @param $headline
     * @return mixed|void
     */
    public function setHeadline($headline)
    {
        $this->setDataInternal('headline', $headline);
    }

    /**
     * @param array $image
     * @return mixed|void
     */
    public function setImage(array $image)
    {
        $this->setDataInternal('image', $image);
    }

    /**
     * @param $date
     * @return mixed|void
     * @throws \yii\base\InvalidConfigException
     */
    public function setDatePublished($date)
    {
        $this->setDataInternal('datePublished', Yii::$app->formatter->asDatetime($date, 'yyyy-MM-dd\'T\'HH:mm:ssZ'));
    }

    /**
     * @param $date
     * @return mixed|void
     * @throws \yii\base\InvalidConfigException
     */
    public function setDateModified($date)
    {
        $this->setDataInternal('dateModified', Yii::$app->formatter->asDatetime($date, 'yyyy-MM-dd\'T\'HH:mm:ssZ'));
    }

    /**
     * @param IPerson $person
     * @return mixed|void
     */
    public function setAuthor(IPerson $person)
    {
        $this->setDataInternal('author', $person);
    }

    /**
     * @param IOrganization $organization
     * @return mixed|void
     */
    public function setPublisher(IOrganization $organization)
    {
        $this->setDataInternal('publisher', $organization);
    }

    /**
     * @param $description
     * @return mixed|void
     */
    public function setDescription($description)
    {
        $this->setDataInternal('description', $description);
    }
}
