<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo\ldjson;

use code2magic\seo\ldjson\interfaces\IThing;

/**
 * Class Thing
 * @package code2magic\seo\ldjson
 */
class Thing extends BaseLdJson implements IThing
{
    /**
     * @return string
     */
    public function getType(): string
    {
        return 'Thing';
    }

    /**
     * @param $name
     * @return mixed|void
     */
    public function setName($name)
    {
        $this->setDataInternal('name', $name);
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->getDataInternal('name');
    }
}
