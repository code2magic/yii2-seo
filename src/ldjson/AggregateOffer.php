<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo\ldjson;

use code2magic\seo\ldjson\interfaces\IAggregateOffer;

/**
 * Class AggregateOffer
 * @package code2magic\seo\ldjson
 */
class AggregateOffer extends BaseLdJson implements IAggregateOffer
{
    /**
     * @param string $currency
     * @return mixed|void
     */
    public function setCurrency(string $currency)
    {
        $this->setDataInternal('priceCurrency', $currency);
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->getDataInternal('priceCurrency');
    }

    /**
     * @param string $price
     * @return mixed|void
     */
    public function setLowPrice(string $price)
    {
        $this->setDataInternal('lowPrice', $price);
    }

    /**
     * @return string
     */
    public function getLowPrice(): string
    {
        return $this->getDataInternal('lowPrice');
    }

    /**
     * @param string $price
     * @return mixed|void
     */
    public function setHighPrice(string $price)
    {
        $this->setDataInternal('highPrice', $price);
    }

    /**
     * @return string
     */
    public function getHighPrice(): string
    {
        return $this->getDataInternal('highPrice');
    }

    /**
     * @param string $count
     * @return mixed|void
     */
    public function setOfferCount(string $count)
    {
        $this->setDataInternal('offerCount', $count);
    }

    /**
     * @return string
     */
    public function getOfferCount(): string
    {
        return $this->getDataInternal('offerCount');
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'AggregateOffer';
    }
}
