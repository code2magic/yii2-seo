<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo\ldjson;

use code2magic\seo\ldjson\interfaces\IWebPage;

/**
 * Class WebPage
 * @package code2magic\seo\ldjson
 */
class WebPage extends BaseLdJson implements IWebPage
{
    /**
     * @return string
     */
    public function getType(): string
    {
        return 'WebPage';
    }

    /**
     * @param $id
     * @return mixed|void
     */
    public function setId($id)
    {
        $this->setDataInternal('@id', $id);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->getDataInternal('@id');
    }
}
