<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo\ldjson;

use code2magic\seo\ldjson\interfaces\IPerson;
use code2magic\seo\ldjson\interfaces\IRating;
use code2magic\seo\ldjson\interfaces\IReview;
use Yii;

/**
 * Class Review
 * @package code2magic\seo\ldjson
 */
class Review extends BaseLdJson implements IReview
{
    /**
     * @return string
     */
    public function getType(): string
    {
        return 'Review';
    }

    /**
     * @return IPerson
     */
    public function getAuthor(): IPerson
    {
        return $this->getDataInternal('author');
    }

    /**
     * @param IPerson $author
     * @return mixed|void
     */
    public function setAuthor(IPerson $author)
    {
        $this->setDataInternal('author', $author);
    }

    /**
     * @return mixed
     */
    public function getDatePublished()
    {
        return $this->getDataInternal('datePublished');
    }

    /**
     * @param $date
     *
     * @throws \yii\base\InvalidConfigException
     */
    public function setDatePublished($date)
    {
        $date = Yii::$app->formatter->asDatetime($date, 'yyyy-MM-dd\'T\'HH:mm:ssZ');
        $this->setDataInternal('datePublished', $date);
    }

    /**
     * @param $description
     * @return mixed|void
     */
    public function setDescription($description)
    {
        $this->setDataInternal('description', $description);
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->getDataInternal('description');
    }

    /**
     * @param IRating $rating
     * @return mixed|void
     */
    public function setReviewRating(IRating $rating)
    {
        $this->setDataInternal('reviewRating', $rating);
    }

    /**
     * @return IRating
     */
    public function getReviewRating(): IRating
    {
        return $this->getDataInternal('reviewRating');
    }
}
