<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo\ldjson;

use catalog\frontend\models\Category;
use catalog\frontend\models\Product;
use code2magic\seo\ldjson\interfaces\ILdJson;
use code2magic\seo\ldjson\interfaces\IOffer;
use code2magic\seo\ldjson\interfaces\IPostalAddress;
use code2magic\seo\ldjson\interfaces\IShop;
use comments\models\CommentModel;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\View;

/**
 * Class LdJson
 * @package code2magic\seo\ldjson
 */
class LdJson implements IShop
{
    /**
     * @var
     */
    private $view;

    /**
     * LdJson constructor.
     * @param View $view
     */
    public function __construct(View $view)
    {
        $this->view = $view;
    }

    /**
     * @param \catalog\frontend\models\Category $category
     *
     * @param                                   $count
     * @param null $price
     *
     * @throws \yii\db\Exception
     */
    public function registerCategory(Category $category, $count, $price = null)
    {
        $product = new \code2magic\seo\ldjson\Product(true);
        $product->setName($category->name);
        if (!$price) {
            $price = Product::getFilter(['path_id-catalogProductToCategoriesPath' => $category->id,])
                ->withDiscount()
                ->getPriceInterval(Yii::$app->currency->current->code);
        }
        // aggregateOffer
        $aggregateOffer = new AggregateOffer();
        $aggregateOffer->setCurrency(Yii::$app->currency->current->code);
        $aggregateOffer->setLowPrice((string)$price['min']);
        $aggregateOffer->setHighPrice((string)$price['max']);
        $aggregateOffer->setOfferCount($count);
        $product->setAggregateOffer($aggregateOffer);
        $comment = CommentModel::find()->approved()->andWhere(['>', 'rating', 0,])->ratingForCategory($category->id);
        if ($comment['ratingCount'] > 0) {
            $aggregateRating = new AggregateRating();
            $aggregateRating->setRatingCount($comment['ratingCount']);
            $aggregateRating->setRatingValue($comment['ratingValue']);
            $product->setAggregateRating($aggregateRating);
        }
        $this->register($product);
    }

    /**
     * @param ILdJson $json
     */
    protected function register(ILdJson $json)
    {
        $this->view->params['ld_json'][] = $json->getLdJsonArray();
    }

    /**
     * @param \catalog\frontend\models\Product $item
     *
     * @throws \League\Flysystem\FileNotFoundException
     * @throws \yii\base\InvalidConfigException
     */
    public function registerProduct(Product $item)
    {
        $product = new \code2magic\seo\ldjson\Product(true);
        $product->setName($item->name);
        $images = [Yii::$app->glide->getGlideThumbnail($item->image->image, 'product_medium'),];
        foreach ($item->images as $image) {
            $images[] = Yii::$app->glide->getGlideThumbnail($image['image'], 'product_medium');
        }
        $product->setImages($images);
        $product->setDescription($item->getMetaData('seo_description'));
        $product->setMpn($item->sku);
        // brand
        $brand = new Thing();
        $brand->setName(Yii::$app->keyStorage->get('common.shop_name'));
        $product->setBrand($brand);
        // offer
        $offer = new Offer();
        $offer->setPriceCurrency(Yii::$app->currency->current->code);
        $offer->setPrice($item->getCost(true, null, false));
        $offer->setAvailability($item->getAvailability() ? IOffer::IN_STOCK : IOffer::OUT_OF_STOCK);
        // seller
        $seller = new Organization();
        $seller->setName((string)Yii::$app->keyStorage->get('common.shop_name'));
        $offer->setSeller($seller);
        $product->setOffer($offer);
        $comments = CommentModel::find()
            ->approved()
            ->andWhere(['entity' => Product::getCommentEntity(), 'entityId' => $item->id,])
            ->andWhere(['>', 'rating', 0,])
            ->asArray()
            ->all();
        if (count($comments) > 0) {
            $ratings = ArrayHelper::getColumn($comments, 'rating');
            $count = count($comments);
            $total = 0;
            if ($count) {
                $total = array_sum($ratings) / $count;
            }
            // aggregate
            $aggregate = new AggregateRating();
            $aggregate->setRatingValue($total);
            $aggregate->setRatingCount($count);
            $product->setAggregateRating($aggregate);
            // reviews
            $reviews = [];
            foreach ($comments as $comment) {
                $review = new Review();
                // author
                $author = new Person();
                $author->setName($comment['name']);
                $review->setAuthor($author);
                $review->setDatePublished($comment['updatedAt']);
                $review->setDescription($comment['content']);
                // rating
                $rating = new Rating();
                $rating->setRatingValue($comment['rating']);
                $review->setReviewRating($rating);
                $reviews[] = $review;
            }
            $product->setReviews($reviews);
        }
        $this->register($product);
    }

    /**
     * @param $data
     * @return mixed|void
     */
    public function registerBreadcrumbs($data)
    {
        $ld = [];
        foreach ($data as $i => $bread) {
            $item = new ListItem();
            $item->setPosition($i + 1);
            $item->setName(is_array($bread) ? $bread['label'] : $bread);
            $item->setItem(
                is_array($bread) ? \yii\helpers\Url::toRoute($bread['url'], true) : \yii\helpers\Url::current([], true)
            );
            $ld[] = $item;
        }
        $bread = new BreadcrumbList(true);
        $bread->setItemListElement($ld);
        $this->register($bread);
    }

    /**
     * @param array $data
     * @return mixed|void
     */
    public function registerContactPage($data = [])
    {
        $this->registerIndexWithoutRatingAndComments($data);
    }

    /**
     * @param $data
     */
    public function registerIndexWithoutRatingAndComments($data)
    {
        $organization = new Organization(true);
        $organization->setName(Yii::$app->keyStorage->get('common.shop_name'));
        //$organization->setPostalAddress($this->getPostalAddress());
        $organization->setUrl(Url::toRoute(['/',], true));
        $organization->setSameAs($this->getSocilaLinks());
        $organization->setLogo(Url::toRoute('img/logo.png', true));
        if(isset($data['telephone'])) {
            $organization->setContactPoint($this->getContactPoint($data['telephone']));
        }
        $this->register($organization);
    }

    /**
     * @return array
     */
    protected function getSocilaLinks()
    {
        $links = [];
        if (Yii::$app->keyStorage->get('frontend.group-url-facebook', false)) {
            $links[] = Yii::$app->keyStorage->get('frontend.group-url-facebook', false);
        }
        if (Yii::$app->keyStorage->get('frontend.group-url-instagram', false)) {
            $links[] = Yii::$app->keyStorage->get('frontend.group-url-instagram', false);
        }
        if (Yii::$app->keyStorage->get('frontend.group-url-twitter', false)) {
            $links[] = Yii::$app->keyStorage->get('frontend.group-url-twitter', false);
        }
        if (Yii::$app->keyStorage->get('frontend.group-url-google', false)) {
            $links[] = Yii::$app->keyStorage->get('frontend.group-url-google', false);
        }
        if (Yii::$app->keyStorage->get('frontend.group-url-youtube', false)) {
            $links[] = Yii::$app->keyStorage->get('frontend.group-url-youtube', false);
        }
        if (Yii::$app->keyStorage->get('frontend.group-url-pinterest', false)) {
            $links[] = Yii::$app->keyStorage->get('frontend.group-url-pinterest', false);
        }
        return $links;
    }

    /**
     * @param $telephone
     * @return array
     */
    protected function getContactPoint($telephone): array
    {
        $point = new ContactPoint();
        $point->setTelephone($telephone);
        $point->setContactType('customer service');
        return [$point,];
    }

    /**
     * @param \werewolf8904\cmscontent\frontend\models\Article $article
     * @return mixed|void
     * @throws \yii\base\InvalidConfigException
     */
    public function registerArticle($article)
    {
        // newsarticle
        $newsarticle = new NewsArticle(true);
        $newsarticle->setHeadline($article->name);
        $newsarticle->setImage([$article->glideThumbnail,]);
        $newsarticle->setDatePublished($article->published_at);
        $newsarticle->setDateModified($article->updated_at);
        $newsarticle->setDescription($article->getShort());
        // author
        $author = new Person();
        $author->setName(Yii::$app->keyStorage->get('common.shop_name'));
        $newsarticle->setAuthor($author);
        // publisher
        $publisher = new Organization();
        $publisher->setName(Yii::$app->keyStorage->get('common.shop_name'));
        // image
        $image = new ImageObject();
        $image->setUrl(Url::toRoute('img/logo.png', true));
        $publisher->setLogo($image);
        $newsarticle->setPublisher($publisher);
        // page
        $page = new WebPage();
        $page->setId(Url::toRoute(['/content/article/view', 'id' => $article->id,], true));
        $newsarticle->setMainEntityOfPage($page);
        $this->register($newsarticle);
    }

    /**
     * @return IPostalAddress
     */
    protected function getPostalAddress(): IPostalAddress
    {
        $postal = new PostalAddress();
        //$postal->setStreetAddress()
        return $postal;
    }

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function registerIndexWithRatingAndReview()
    {
        $rating_count = (new \yii\db\Query())
            ->select([
                'rating' => new Expression('SUM(comment.rating)/count(comment.id)'),
                'count' => new Expression('count(comment.id)'),
            ])
            ->from('comment')
            ->leftJoin(Product::tableName(), CommentModel::tableName() . '.entityId=' . Product::tableName() . '.id')
            ->where([CommentModel::tableName() . '.status' => 1, Product::tableName() . '.status' => 1,])
            ->all();
        /*$review_product = Product::find()->joinWith('catalogProductCommentsSeo')->andWhere([Product::tableName().'.status'=>1, CommentModel::tableName() . '.status'=>1])->all();*/
        $reviews = CommentModel::find()
            ->joinWith('productsSeo')
            ->andWhere([Product::tableName() . '.status' => 1, CommentModel::tableName() . '.status' => 1,])
            ->all();
        $index_json = [
            '@context' => 'http://schema.org',
            '@type' => 'Organization',
            'name' => Yii::t('frontend.seo', 'organization'),
        ];
        $index_json["address"] = [
            '@type' => 'PostalAddress',
            'streetAddress' => Yii::t('frontend.seo', 'address'),
            'addressLocality' => Yii::t('frontend.seo', 'city'),
            'addressRegion' => Yii::t('frontend.seo', 'area'),
            'postalCode' => '29000',
            'addressCountry' => 'UA',
        ];
        $index_json["aggregateRating"] = [
            '@type' => 'AggregateRating',
            'ratingValue' => $rating_count[0]['rating'],
            'ratingCount' => $rating_count[0]['count'],
        ];
        foreach ($reviews as $key => $val_comment) {
            $index_json['review'][] = [
                '@type' => 'Review',
                'url' => Url::toRoute(['/catalog/catalog/product', 'id' => $val_comment->entityId,], true),
                'author' => [
                    '@type' => 'Person',
                    'name' => $val_comment->name,
                ],
                'datePublished' => Yii::$app->formatter->asDatetime($val_comment->createdAt),
                'description' => $val_comment->content,
                'reviewRating' => [
                    '@type' => 'Rating',
                    'worstRating' => 1,
                    'bestRating' => 5,
                    'ratingValue' => $val_comment->rating,
                ],
            ];
        }
        $index_json['url'] = Url::toRoute(['/',], true);
        $index_json['logo'] = Url::toRoute('img/logo.png', true);
        $index_json['contactPoint'] = [
            '@type' => 'ContactPoint',
            'telephone' => Yii::t('frontend.seo', 'ContactPoint.telephone'),
            'contactType' => 'customer service',
        ];
        $this->view->params['ld_json'][] = $index_json;
    }
}
