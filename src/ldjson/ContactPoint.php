<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo\ldjson;

use code2magic\seo\ldjson\interfaces\IContactPoint;

/**
 * Class ContactPoint
 * @package code2magic\seo\ldjson
 */
class ContactPoint extends BaseLdJson implements IContactPoint
{
    /**
     * @param $phone
     * @return mixed|void
     */
    public function setTelephone($phone)
    {
       $this->setDataInternal('telephone',$phone);
    }

    /**
     * @return mixed
     */
    public function getTelephone()
    {
        return $this->getDataInternal('telephone');
    }

    /**
     * @param $phone
     * @return mixed|void
     */
    public function setContactType($phone)
    {
        $this->setDataInternal('contactType',$phone);
    }

    /**
     * @return mixed
     */
    public function getContactType()
    {
       return $this->getDataInternal('contactType');
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'ContactPoint';
    }
}
