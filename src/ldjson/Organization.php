<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo\ldjson;

use code2magic\seo\ldjson\interfaces\IAggregateRating;
use code2magic\seo\ldjson\interfaces\IOrganization;
use code2magic\seo\ldjson\interfaces\IPostalAddress;
use code2magic\seo\ldjson\interfaces\IReview;

/**
 * Class Organization
 *
 * @package code2magic\seo\ldjson
 */
class Organization extends BaseLdJson implements IOrganization
{
    /**
     * @return string
     */
    public function getType(): string
    {
        return 'Organization';
    }

    /**
     * @param string $name
     *
     * @return mixed|void
     */
    public function setName(string $name)
    {
        $this->setDataInternal('name', $name);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->getDataInternal('name');
    }

    /**
     * @param string $name
     *
     * @return mixed|void
     */
    public function setUrl(string $name)
    {
        $this->setDataInternal('url', $name);
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->getDataInternal('url');
    }

    /**
     * @param array $same
     *
     * @return mixed|void
     */
    public function setSameAs(array $same)
    {
        $this->setDataInternal('sameAs', $same);
    }

    /**
     * @return array
     */
    public function getSameAs(): array
    {
        return $this->getDataInternal('sameAs');
    }

    /**
     * @param interfaces\IImageObject|string $logo_url
     */
    public function setLogo($logo_url)
    {
        $this->setDataInternal('logo', $logo_url);
    }

    /**
     * @return interfaces\IImageObject|mixed|string
     */
    public function getLogo()
    {
        return $this->getDataInternal('logo');
    }

    /**
     * @param IPostalAddress $address
     *
     * @return mixed|void
     */
    public function setPostalAddress(IPostalAddress $address)
    {
        $this->setDataInternal('address', $address);
    }

    /**
     * @return IPostalAddress
     */
    public function getPostalAddress(): IPostalAddress
    {
        return $this->getDataInternal('address');
    }

    /**
     * @param array $contactPoint
     *
     * @return mixed|void
     */
    public function setContactPoint(array $contactPoint)
    {
        $this->setDataInternal('contactPoint', $contactPoint);
    }

    /**
     * @return array
     */
    public function getContactPoint(): array
    {
        return $this->getDataInternal('contactPoint');
    }

    /**
     * @param IAggregateRating $aggregateRating
     */
    public function setAggregateRating(IAggregateRating $aggregateRating)
    {
        $this->setDataInternal('aggregateRating', $aggregateRating);
    }

    /**
     * @return IAggregateRating
     */
    public function getAggregateRating(): IAggregateRating
    {
        return $this->getDataInternal('aggregateRating');
    }

    /**
     * @param IReview[] $reviews
     */
    public function setReviews(array $reviews)
    {
        return $this->setDataInternal('review', $reviews);
    }

    /**
     * @return IReview[]
     */
    public function getReviews()
    {
        return $this->getDataInternal('review');
    }
}
