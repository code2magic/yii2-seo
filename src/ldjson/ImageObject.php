<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo\ldjson;

use code2magic\seo\ldjson\interfaces\IImageObject;

/**
 * Class ImageObject
 * @package code2magic\seo\ldjson
 */
class ImageObject extends BaseLdJson implements IImageObject
{
    /**
     * @param $url
     * @return mixed|void
     */
    public function setUrl($url)
    {
        $this->setDataInternal('url', $url);
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'ImageObject';
    }
}
