<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo\ldjson;

use code2magic\seo\ldjson\interfaces\IOffer;
use code2magic\seo\ldjson\interfaces\IOrganization;

/**
 * Class Offer
 *
 * @package code2magic\seo\ldjson
 */
class Offer extends BaseLdJson implements IOffer
{
    /**
     * @return string
     */
    public function getType(): string
    {
        return 'Offer';
    }

    /**
     * @return mixed
     */
    public function getPriceCurrency()
    {
        return $this->getDataInternal('priceCurrency');
    }

    /**
     * @param $currency
     *
     * @return mixed|void
     */
    public function setPriceCurrency($currency)
    {
        $this->setDataInternal('priceCurrency', $currency);
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->getDataInternal('price');
    }

    /**
     * @param $price
     *
     * @return mixed|void
     */
    public function setPrice($price)
    {
        $price = number_format($price, 2, '.', '');
        $this->setDataInternal('price', $price);
    }

    /**
     * @param $availability
     *
     * @return mixed|void
     */
    public function setAvailability($availability)
    {
        $this->setDataInternal('availability', $availability);
    }

    /**
     * @return mixed
     */
    public function getAvailability()
    {
        return $this->getDataInternal('availability');
    }

    /**
     * @return IOrganization
     */
    public function getSeller(): IOrganization
    {
        return $this->getDataInternal('seller');
    }

    /**
     * @param IOrganization $seller
     *
     * @return mixed|void
     */
    public function setSeller(IOrganization $seller)
    {
        $this->setDataInternal('seller', $seller);
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->getDataInternal('url');
    }

    /**
     * @param string $value
     *
     * @return mixed|void
     */
    public function setUrl(string $value)
    {
        $this->setDataInternal('url', $value);
    }

    /**
     * @return string
     */
    public function getPriceValidUntil(): string
    {
        return $this->getDataInternal('priceValidUntil');
    }

    /**
     * @param string $value
     *
     * @return mixed|void
     */
    public function setPriceValidUntil($value): string
    {
        $this->setDataInternal('priceValidUntil', $value);
    }

    /**
     * @return string
     */
    public function getItemCondition(): string
    {
        return $this->getDataInternal('itemCondition');
    }

    /**
     * @param string $value
     */
    public function setItemCondition(string $value)
    {
        $this->setDataInternal('itemCondition', $value);
    }
}
