<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo\ldjson;

use code2magic\seo\ldjson\interfaces\IAggregateRating;

/**
 * Class AggregateRating
 *
 * @package code2magic\seo\ldjson
 */
class AggregateRating extends BaseLdJson implements IAggregateRating
{
    /**
     * @param $value
     *
     * @return mixed|void
     */
    public function setRatingValue($value)
    {
        $this->setDataInternal('ratingValue', (float)$value);
    }

    /**
     * @return mixed
     */
    public function getRatingValue()
    {
        return $this->getDataInternal('ratingValue');
    }

    /**
     * @param $value
     *
     * @return mixed|void
     */
    public function setRatingCount($value)
    {
        $this->setDataInternal('ratingCount', $value);
    }

    /**
     * @return mixed
     */
    public function getRatingCount()
    {
        return $this->getDataInternal('ratingCount');
    }

    /**
     * @return mixed
     */
    public function getWorstRating()
    {
        return $this->getDataInternal('worstRating');
    }

    /**
     * @param $value
     */
    public function setWorstRating($value)
    {
        $this->setDataInternal('worstRating', $value);
    }

    /**
     * @return mixed
     */
    public function getBestRating()
    {
        return $this->getDataInternal('bestRating');
    }

    /**
     * @param $value
     */
    public function setBestRating($value)
    {
        $this->setDataInternal('bestRating', $value);
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'AggregateRating';
    }
}
