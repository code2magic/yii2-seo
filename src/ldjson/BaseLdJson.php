<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo\ldjson;

use code2magic\seo\ldjson\interfaces\ILdJson;
use yii\helpers\ArrayHelper;

/**
 * Class BaseLdJson
 * @package code2magic\seo\ldjson
 */
abstract class BaseLdJson implements ILdJson
{
    /**
     * @var bool
     */
    protected $_is_context;

    /**
     * @var
     */
    private $_data;

    /**
     * BaseLdJson constructor.
     * @param bool $is_context
     */
    public function __construct($is_context = false)
    {
        $this->_is_context = $is_context;
        if ($this->_is_context) {
            $this->_data['@context'] = 'http://schema.org';
        }
        $this->_data['@type'] = $this->getType();
    }

    /**
     * @return array|mixed
     */
    public function jsonSerialize()
    {
        return $this->getLdJsonArray();
    }

    /**
     * @param bool $isContext
     * @return mixed|void
     */
    public function setIsContext(bool $isContext)
    {
        $this->_is_context = $isContext;
    }

    /**
     * @param $name
     * @param $value
     */
    protected function setDataInternal($name, $value)
    {
        if ($value instanceof ILdJson) {
            $value = $value->getLdJsonArray();
        }
        $this->_data[$name] = $value;
    }

    /**
     * @return bool
     */
    public function getIsContext(): bool
    {
        return $this->_is_context;
    }

    /**
     * @param $name
     * @return mixed
     */
    protected function getDataInternal($name)
    {
        return ArrayHelper::getValue($this->_data, $name);
    }

    /**
     * @return array
     */
    public function getLdJsonArray(): array
    {
        return $this->_data;
    }
}
