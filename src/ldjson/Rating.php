<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo\ldjson;

use code2magic\seo\ldjson\interfaces\IRating;

/**
 * Class Rating
 * @package code2magic\seo\ldjson
 */
class Rating extends BaseLdJson implements IRating
{
    /**
     * Rating constructor.
     * @param bool $is_context
     */
    public function __construct($is_context = false)
    {
        parent::__construct($is_context);
        $this->setDataInternal('worstRating', 1);
        $this->setDataInternal('bestRating', 5);
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'Rating';
    }

    /**
     * @return int|mixed
     */
    public function getBestRating()
    {
        return 5;
    }

    /**
     * @param $rating
     * @return mixed|void
     */
    public function setBestRating($rating)
    {
        // TODO: Implement setBestRating() method.
    }

    /**
     * @param $rating
     * @return mixed|void
     */
    public function setRatingValue($rating)
    {
        $this->setDataInternal('ratingValue', $rating);
    }

    /**
     * @return mixed
     */
    public function getRatingValue()
    {
        return $this->getDataInternal('ratingValue');
    }

    /**
     * @return int|mixed
     */
    public function getWorstRating()
    {
        return 1;
    }

    /**
     * @param $rating
     * @return mixed|void
     */
    public function setWorstRating($rating)
    {
        // TODO: Implement setWorstRating() method.
    }
}
