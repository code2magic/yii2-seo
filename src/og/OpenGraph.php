<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo\og;

use catalog\frontend\models\Category;
use catalog\frontend\models\Product;
use common\helpers\GlideThumbnailHelper;
use Yii;
use yii\helpers\Url;

/**
 * Class OpenGraph
 * @package code2magic\seo\og
 */
class OpenGraph
{
    /**
     * @param Product $product
     */
    public function registerProduct(Product $product)
    {
        $data = $this->initOg();
        $data['og:title'] = $product->getMetaData('seo_title');
        $data['og:description'] = $product->getMetaData('seo_description');
        $data['og:image'] = GlideThumbnailHelper::getGlideThumbnail($product->image->image, GlideThumbnailHelper::OG_SMALL);
        $this->register($data);
    }

    /**
     * @param Category $category
     */
    public function registerCategory(Category $category)
    {
        $data = $this->initOg();
        $data['og:title'] = $category->getMetaData('seo_title');
        $data['og:description'] = $category->getMetaData('seo_description');
        $data['og:image'] = GlideThumbnailHelper::getGlideThumbnail($category->image, GlideThumbnailHelper::OG_SMALL);
        $this->register($data);
    }

    /**
     *
     */
    public function registerIndex()
    {
        $data = $this->initOg();
        $data['og:title'] = Yii::t('key_message', 'seo_home_title', ['shop' => Yii::$app->keyStorage->get('common.shop_name')]);
        $data['og:description'] = Yii::t('key_message', 'seo_home_description', ['shop' => Yii::$app->keyStorage->get('common.shop_name')]);
        $this->register($data);
    }

    /**
     * @return array
     */
    protected function initOg()
    {
        return [
            'og:url' => Url::canonical(),
            'og:type' => 'website',
            'og:site_name' => Yii::$app->keyStorage->get('common.shop_name'),
            'og:image' =>GlideThumbnailHelper::getGlideThumbnail('logo.png', GlideThumbnailHelper::OG_SMALL)
        ];
    }

    /**
     * @param $data
     */
    protected function register($data)
    {
        foreach ($data as $k => $d) {
            Yii::$app->view->registerMetaTag(['property' => $k, 'content' => $d]);
        }
    }

    /**
     * @param $title
     * @param $description
     */
    public function registerPage($title,$description)
    {
        $data = $this->initOg();
        $data['og:title'] = $title;
        $data['og:description'] = $description;
        $this->register($data);
    }

}