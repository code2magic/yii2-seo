<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo;

use yii\helpers\ArrayHelper;

/**
 * Class IndexFollowClarifier
 * @package code2magic\seo
 */
class IndexFollowClarifier implements IndexFollowClarifierInterface
{
    /**
     * @var array
     */
    public $robots_map = [
        'catalog/catalog/category' => [
            'id' => [
                'index' => true,
                'follow' => true,
            ],
            'page' => [
                'index' => false,
                'follow' => true,
            ],
            'filter' => [
                'index' => false,
                'follow' => false,
            ],
        ],
        'catalog/catalog/product' => [
            'id' => [
                'index' => true,
                'follow' => true,
            ],
        ],
        'site/index' => [
            true,
        ],
        'content/page/view' => [
            'id' => [
                'index' => true,
                'follow' => true,
            ],
        ],
        'content/article/view' => [
            'id' => [
                'index' => true,
                'follow' => true,
            ],
        ],
        'content/article/index' => [
            true,
            'id' => [
                'index' => true,
                'follow' => true,
            ],
        ],
    ];

    /**
     * @var
     */
    protected $_route;

    /**
     * @var array
     */
    protected $_params = [];

    /**
     * @var bool
     */
    protected $_index;

    /**
     * @var bool
     */
    protected $_follow;

    /**
     * IndexFollowClarifier constructor.
     * @param string $route
     * @param array $params
     */
    public function __construct(string $route, array $params)
    {
        $this->_params = $params;
        $this->_route = $route;
    }

    /**
     *
     */
    protected function clarify(): void
    {
        $this->_follow = true;
        $this->_index = true;
        $map = ArrayHelper::getValue($this->robots_map, $this->_route);
        if ($map) {
            foreach ($this->_params as $name => $param) {
                $p = ArrayHelper::getValue($map, $name);
                if ($p) {
                    $this->_follow = $this->_follow && $p['follow'];
                    $this->_index = $this->_index && $p['index'];
                } else {
                    $this->_follow = false;
                    $this->_index = false;
                    break;
                }
            }
        } else {
            $this->_follow = false;
            $this->_index = false;
        }
    }

    /**
     * @return bool
     */
    public function isIndex(): bool
    {
        if($this->_index === null){
            $this->clarify();
        }
        return $this->_index;
    }

    /**
     * @return bool
     */
    public function isFollow(): bool
    {
        if($this->_follow === null){
            $this->clarify();
        }
        return $this->_follow;
    }
}
