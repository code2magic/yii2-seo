<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo;

use code2magic\seo\og\OpenGraph;
use werewolf8904\cmscontent\frontend\controllers\ArticleController;
use werewolf8904\cmscontent\frontend\controllers\PageController;
use werewolf8904\cmscore\components\View;
use Yii;
use yii\base\Behavior;
use yii\helpers\ArrayHelper;

/**
 * Class SeoBeginPageBehavior
 * @package code2magic\seo
 */
class SeoBeginPageBehavior extends Behavior
{
    /**
     * {@inheritdoc}
     */
    public function attach($owner)
    {
        $this->owner = $owner;
        $owner->on(View::EVENT_BEFORE_PAGE_RENDER, [$this, 'before',]);
    }

    /**
     * {@inheritdoc}
     */
    public function detach()
    {
        if ($this->owner) {
            $this->owner->off(View::EVENT_BEFORE_PAGE_RENDER, [$this, 'before',]);
            $this->owner = null;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function before($event)
    {
        $context = $event->sender->context;
        if ($context instanceof PageController) {
            $og = new OpenGraph();
            /**
             * @var $view View
             */
            $view = $this->owner;
            /**
             * @var $page \werewolf8904\cmscontent\frontend\models\Page
             */
            $page = ArrayHelper::getValue($event->params, 'model');
            $decription = $page->seo_description;
            if (!$decription) {
                $decription = Yii::t('key_message', 'seo_page_description', ['title' => $view->title,]);
                $view->registerMetaTag(['name' => 'description', 'content' => $decription,], 'description');
            }
            $og->registerPage($view->title, $decription);
        }
        if ($context instanceof ArticleController) {
            $og = new OpenGraph();
            /**
             * @var $view View
             */
            $view = $this->owner;
            /**
             * @var $page \werewolf8904\cmscontent\frontend\models\ArticleCategory
             */
            $articlecategory = ArrayHelper::getValue($event->params, 'model');
            $decription = $articlecategory->seo_description;
            if (!$decription) {
                $template = $context->action->actionMethod === 'actionIndex' ? 'seo_article_category_description_template' : 'seo_article__description_template';
                $decription = Yii::t('key_message', $template, ['title' => $view->title,]);
                $view->registerMetaTag(['name' => 'description', 'content' => $decription,], 'description');
            }
            $og->registerPage($view->title, $decription);
        }
    }
}