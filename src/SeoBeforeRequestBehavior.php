<?php
/**
 * @author Werewolf
 */

namespace code2magic\seo;

use Yii;
use yii\base\Behavior;
use yii\web\Application;

/**
 * Class SeoBeforeRequestBehavior
 * @package code2magic\seo
 */
class SeoBeforeRequestBehavior extends Behavior
{
    /**
     * {@inheritdoc}
     */
    public function attach($owner)
    {
        $this->owner = $owner;
        $owner->on(Application::EVENT_BEFORE_ACTION, [$this, 'beforeAction',]);
    }

    /**
     * {@inheritdoc}
     */
    public function detach()
    {
        if ($this->owner) {
            $this->owner->off(Application::EVENT_BEFORE_ACTION, [$this, 'beforeAction',]);
            $this->owner = null;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function beforeAction($action)
    {
        if ( Yii::$app->request->isGet && (!(Yii::$app->request->isAjax || Yii::$app->request->isPjax))) {
            $route = Yii::$app->controller->route;
            $clarifier = Yii::createObject(IndexFollowClarifierInterface::class, [$route, Yii::$app->request->getQueryParams(),]);
            Yii::$app->view->params['robots_index'] = $clarifier->isIndex();
            Yii::$app->view->params['robots_follow'] = $clarifier->isFollow();
        }
        return true;
    }
}